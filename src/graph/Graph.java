/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graph;

/**
 *
 * @author Molchac
 */

import java.util.ArrayList;
import java.util.List;
public class Graph {

    private List<Node> nodes;
public void addNode(Node node) {
if (nodes == null) {
nodes = new ArrayList<>();
}
nodes.add(node);
}
public List<Node> getNodes() {
return nodes;
}
@Override
public String toString() {
return "Graph [nodes=" + nodes + "]";
}

public static Graph getCities() {
    Node camp = new Node("Campeche");
    Node tena = new Node("Tenabo");
    
    camp.addEdge(new Edge(camp, tena, 40));
    
    Graph graph = new Graph();
    graph.addNode(camp);
    graph.addNode(tena);
    return graph;
}

    public static void main(String[] args) {
        Graph graph = getCities();
        System.out.println(graph);
        
        
        
    }
    
}
